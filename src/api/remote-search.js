import request from '@/utils/request'

export function searchUser(name) {
  return request({
    url: '/user/patients',
    method: 'get',
    params: { name }
  })
}

export function searchPatient(name) {
  return request({
    url: '/user/patients',
    method: 'get',
    params: { name }
  })
}

export function searchPractitioner(name) {
  return request({
    url: '/user/practitioners',
    method: 'get',
    params: { name }
  })
}

export function transactionList(query) {
  return request({
    url: '/vue-element-admin/transaction/list',
    method: 'get',
    params: query
  })
}
