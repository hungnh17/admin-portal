import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/user/patients',
    method: 'get',
    params: query
  })
}

export function fetchPatient(id) {
  return request({
    url: '/user/patients/' + id,
    method: 'get'
  })
}

export function createPatient(data) {
  return request({
    url: '/user/patients',
    method: 'post',
    data
  })
}

export function deletePatient(id) {
  return request({
    url: '/user/patients/' + id,
    method: 'delete'
  })
}

export function updatePatient(id, data) {
  let generalData = {
    phoneNumber: data.phoneNumber,
    familyName: data.familyName,
    givenName: data.givenName,
    prefixName: data.prefixName,
    suffixName: data.suffixName,
    gender: data.gender,
    birthDate: data.birthDate,
    maritalStatus: data.maritalStatus,
    managingOrganization: data.managingOrganization,
  }
  return request({
    url: '/user/patients/' + id,
    method: 'put',
    data: generalData
  })
}
