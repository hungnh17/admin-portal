export default {
  route: {
    dashboard: 'Dashboard',
    profile: 'Profile',
    patient: 'Patient',
    practitioner: 'Practitioner',
    appointment: 'Appointment',
    create_patient: 'Create Patient',
    patient_list: 'Patient List',
    create_practitioner: 'Create Practitioner',
    practitioner_list: 'Practitioner List',
    edit_practitioner: 'Edit',
  },
  navbar: {
    profile: 'Profile',
    dashboard: 'Dashboard',
    logOut: 'Log Out'
  },
  login: {
    title: 'Login Form',
    username: 'Username',
    password: 'Password',
    logIn: 'Login'
  },
  patient: {
    userInformation: 'User Infomation',
    prefixName: 'Prefix Name',
    givenName: 'Given Name',
    suffixName: 'Suffix Name',
    familyName: 'Family Name',
    email: 'Email',
    gender: 'Gender',
    phoneNumber: 'Phone Number',
    birthDate: 'Birth Date',
    managingOrganization: 'Managing Organization',
    maritalStatus: 'Marital Status',
    identifierInfomation: 'Identifier Infomation',
    identifierType: 'Identifier Type',
    identifierValue: 'Value',
    identifierStartValid: 'Start Valid',
    identifierEndValid: 'End Valid',
    addressInfomation: 'Address Infomation',
    addressUse: 'Use',
    addressText: 'Text',
    addressDistrict: 'District',
    addressCity: 'City',
    contactInformation: 'Contact Information',
  },
  practitioner: {
    title: 'Login Form',
    userInformation: 'User Infomation',
    prefixName: 'Prefix Name',
    givenName: 'Given Name',
    suffixName: 'Suffix Name',
    familyName: 'Family Name',
    email: 'Email',
    gender: 'Gender',
    phoneNumber: 'Phone Number',
    birthDate: 'Birth Date',
    managingOrganization: 'Managing Organization',
    maritalStatus: 'Marital Status',
    experience: 'Experience',
    specialty: 'Specialty',
    qualifications: 'Qualification',
    qualificationCode: "Code",
    qualificationName: "Name",
    qualificationIssuer: "Issuer",
    qualificationStartValid: "Start Valid",
    qualificationEndValid: "End Valid",
    identifierInfomation: 'Identifier Infomation',
    identifierType: 'Identifier Type',
    identifierValue: 'Value',
    identifierStartValid: 'Start Valid',
    identifierEndValid: 'End Valid',
    addressInfomation: 'Address Infomation',
    addressUse: 'Use',
    addressText: 'Text',
    addressDistrict: 'District',
    addressCity: 'City',
  },
  appointment: {
    id: 'Id',
    description: 'Description',
    startDate: 'Start Date',
    endDate: 'End Date',
    status: 'Status',
    patient: 'Patient',
    practitioner: 'Practitioner',
    minutesDuration: 'Minutes Duration',
    patientInstruction: 'Patient Instruction',
    supportingInformation: 'Supporting Information'
  },
  form: {
    create: 'Create',
    update: 'Update',
    action: 'Action',
    search: 'Search',
    add: 'Add',
    reset: 'Reset',
    edit: 'Edit',
    delete: 'Delete',
    cancel: 'Cancel',
    confirm: 'Confirm',
    search_patient: 'Search patient',
    search_practitioner: 'Search practitioner',
    input: 'Please input',
  },
  table: {
    edit: 'Edit',
    action: 'Actions',
  },
  validation: {
    empty_field: "This field cant be empty",
    empty_mail: "mail can't be empty",
    invalid_mail: "Please enter the correct mailbox format",
    empty_phone: "phone number cannot be empty",
    integer_phone: "Please enter a numeric value",
    invalid_phone: "phone number format is incorrect",
    invalid_start_end_time: "Start time must be earlier than end time",
  }
}
